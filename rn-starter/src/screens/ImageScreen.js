import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import ImageDetail from '../components/ImageDetail';

const ImageScreen = () => {
    return(
        <View>
            <ImageDetail title="beach" imageSource={require('../../assets/beach.jpg')} imageScore={9}/>
            <ImageDetail title="forest" imageSource={require('../../assets/forest.jpg')} imageScore={7}/>
            <ImageDetail title="mountain" imageSource={require('../../assets/mountain.jpg')} imageScore={4}/>
        </View>
    );
}

const style = StyleSheet.create({

});

export default ImageScreen;