import React from 'react';
import { Text, View, Button, StyleSheet } from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <View>
      <Button
        title="Go to component demo"
        onPress={() => navigation.navigate('Components')}
      />

      <Button
        title="Go to list demo"
        onPress={() => navigation.navigate('List')}
      />

      <Button
        title="Go to image demo"
        onPress={() => navigation.navigate('Image')}
      />

      <Button
        title="Go to Counter demo"
        onPress={() => navigation.navigate('Counter')}
      />

      <Button
        title="Go to Color demo"
        onPress={() => navigation.navigate('Color')}
      />

      <Button
        title="Go to SquareWithHooks demo"
        onPress={() => navigation.navigate('SquareWithHooks')}
      />

      <Button
        title="Go to Square demo"
        onPress={() => navigation.navigate('Square')}
      />

      <Button
        title="Go to CounterWithReducer demo"
        onPress={() => navigation.navigate('CounterWithReducer')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;
