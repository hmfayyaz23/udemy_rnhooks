import React from 'react';
import { Text, View, StyleSheet, FlatList } from 'react-native';

const ListScreen = () => {
    const friends = [
        { name: 'frnd1', age: 20 },
        { name: 'frnd2', age: 30 },
        { name: 'frnd3', age: 20 },
        { name: 'frnd4', age: 40 },
        { name: 'frnd5', age: 20 },
        { name: 'frnd6', age: 20 },
        { name: 'frnd7', age: 80 },
        { name: 'frnd8', age: 20 },
        { name: 'frnd9', age: 50 },
        { name: 'frnd10', age: 60 }
    ];
    return (
        <FlatList
            //horizontal
            //showsHorizontalScrollIndicator={false}
            keyExtractor={friend => (friend.name)}
            data={friends}
            renderItem={({ item }) => {
                return (
                    <Text style={styles.textStyle}>
                        {item.name} - Age {item.age}
                    </Text>
                );
            }}
        />
    );
}

const styles = StyleSheet.create({
    textStyle: {
        marginVertical: 50
    }
});

export default ListScreen;